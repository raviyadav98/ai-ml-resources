# **AI/ML Resources**
## **List of amazing deep neural network blogs:**
1. [Inceptionism: Going Deeper into neural Networks:](https://ai.googleblog.com/2015/06/inceptionism-going-deeper-into-neural.html)
2. [Understanding LSTM Networks](https://colah.github.io/posts/2015-08-Understanding-LSTMs/)
3. [The Unreasonable Effectiveness of Recurrent Neural Networks](http://karpathy.github.io/2015/05/21/rnn-effectiveness/)
4. [Neural Networks, Manifolds, and Topology](https://towardsdatascience.com/recurrent-neural-networks-by-example-in-python-ffd204f99470)


## Few great blogs
1. [Colah's Blog](https://colah.github.io/)

